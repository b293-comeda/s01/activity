package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//This file serves as our index/entry point

//@SpringBootApplication this is called as "Annotation" Mark
//represented by the "@" symbol
//This is a shortcut or method we manually create or describe our purpose or configure a framework
//Annotation are used to provide supplemental information about the program

@SpringBootApplication

//Tells the springboot that this will handle endpoints for web requests
@RestController
//To handle endpoints and methods

public class Wdc044Application {
	//This method starts the whole Springboot Framework
	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	//How to run our application?
	// ./mvnw spring-boot:run

	//Mapping HTTP Get Request
	@GetMapping("/hello")

	//@RequestParam is used to extract query parameters, form parameters, and even files from the request
	//name = Jimboy; Hello Jimboy
	//name = World; Hello World
	//"?" means the start of the parameters followed by "key=value" pair

	//Access query parameter
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "World") String greet){
		return  String.format("Good evening, %s! Welcome to Batch 293!", greet);
	}

	//ACTIVITY 1 STARTS HERE
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "World") String user){
		return String.format("Hi %s!", user);
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam(value = "name", defaultValue = "World") String name, @RequestParam(value = "age", defaultValue = "18") int age){
		return String.format("Hello %s! Your age is %d.", name, age);
	}
	//ACTIVITY 1 ENDS HERE

}
