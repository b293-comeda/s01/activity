package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post){
        //findByUsername to retrieve the user
        //Criteria for finding the user is from the jwtToken method "getUserNameFromToken"
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        // This title and content will come from the reqBody which is passed through the "post" identifier
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());

        //author retrieve from the token
        newPost.setUser(author);

        postRepository.save(newPost);

    }

    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    // Edit a post method
    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        // We will retrieve the post object for updating using the "id" provided.
        Post postForUpdating = postRepository.findById(id).get();

        //Because the relationship is established between the User and Post models, we are able to retrieve the username of the author.
        String postAuthor = postForUpdating.getUser().getUsername();

        // We will retrieve the username of the currently logged-in user from the token.
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        // This will check if the current logged-in user is the owner of the post.
            //If yes/true - user will have access to Post crud operation; update method.
        if(authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully!", HttpStatus.OK);
        }
        // If the username of the logged-in user didn't match with the author, we will send an error message - bad auth.
        else {
           return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    // Delete a post
    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForDeleting = postRepository.findById(id).get();
        String postAuthor = postForDeleting.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("You have deleted your post.", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("You are not authorized to delete this post!", HttpStatus.UNAUTHORIZED);
        }

    }

    public Iterable<Post> getMyPosts(String stringToken) {
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        User userProfile = userRepository.findByUsername(authenticatedUser);
        Long userId = userProfile.getId();
        List<Post> userPosts = postRepository.findByUserId(userId);
        return userPosts;
    }



}
