package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createUser(User user) {
        //Saves the user in our User Table
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    public Object getUser(String stringToken) {
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        User userProfile = userRepository.findByUsername(authenticatedUser);
        userProfile.setPassword("");
        return userProfile;
    }

}
