package com.zuitt.wdc044.models;

import javax.persistence.*;

@Entity

@Table(name="posts")
public class Post {
    @Id
    //indicates that this property represents the primary key
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    // @JoinColumn defines the foreign key in our Post Table.
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    public Post(){

    }

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public User getUser() {
        return user;
    }

    public Long getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
